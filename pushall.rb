#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

def t s 
  p s
  Thread.new { system s }
end

`git remote`.each_line do |x|
  t "git push #{x.strip} master "
end

sleep 0.3 while Thread.list.size != 1
