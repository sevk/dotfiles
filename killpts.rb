#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
#

#ps -t pts/1 | awk '/[0-9]/ {print $1}' | xargs sudo kill -9 
#n = ARGV[0].to_i
#s=`ps -t pts/#{n}`
`w -h`.each_line{|x|
  next if x=~ / tty\d+ /i
  if x !~ /w -h$/
    puts x
    s= `ps -t #{x.match(/pts\/\d+/)[0]}`
    #puts s
    pids = s.scan(/^\s*\d+/mi).join ' '
    `sudo kill -9 #{pids}`
  end
}

