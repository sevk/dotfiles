#!/usr/bin/env ruby
#alsactl -F init # if can't unmute

s = `amixer get Master`
val = s.scan(/Mono:\sPlayback\s(\d+)/)[0][0].to_i
val1 = 41

if val < val1
  `notify-send '<span color="yellow" size="60000">unmute </span>'`
  puts `amixer set Master #{val1} `
else
  puts `amixer set Master 1`
  `notify-send '<span color="yellow" size="60000">mute </span>'`
end

#if not File.exist? '/usr/local/sbin/m'
  #puts `ln -s #{Dir.pwd}/m.rb /usr/local/sbin/m`
#end

