#!/bin/bash
{
    # Log beggining of backup
    /usr/bin/logger $0 `date`
    PATH=/usr/local/sbin:/sbin:/usr/sbin:/bin:/usr/bin 

    iptables -I OUTPUT -j REJECT
    ifconfig eth1 down

    # Log end of backup
    /usr/bin/logger $0 `date`
} &

