#!/usr/bin/env ruby
# 自动查找所有 screen 并 -r , 参数是第几个screen .
# 比如 scr.rb 0 或 scr.rb 1 , 
# 可以 alias s="scr.rb"
# alias s1='scr.rb 1'
# alias s2='scr.rb 2'

n = ARGV[0].to_i
s=`screen -list`
puts s
`screen -rd #{s.scan(/^\s+\d+/mi)[n].to_i}`


