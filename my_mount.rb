#!/usr/bin/ruby
# -*- coding: utf-8 -*-

#set path
a=ENV['PATH']
ENV['PATH'] = "/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/bin"
`/usr/bin/logger [path= #{a}] ... `

src = "/dev/#{ARGV[0]}"
p src
`/usr/bin/logger argv[1] = #{ARGV[1]}]`

class NilClass
  def match a
    nil
  end
end

if ARGV[1] .match /umount/i
  `/bin/umount #{src}`
  `/usr/bin/logger un mount ok `
  exit
end

#get label
label=`blkid #{src}` .scan(/\w+="(.*?)"/i)[0][0]

dir = "/media/kk/#{label}"
`/usr/bin/logger #{src} #{dir}`

Dir.mkdir dir unless File.exist? dir

myname = "kk"

#mount it
`/bin/mount -o iocharset=utf8,umask=022,uid=#{myname} #{src} #{dir}`
`/usr/bin/logger "/bin/mount -o shortname=mixed,iocharset=utf8,umask=022,uid=#{myname} #{src} #{dir}" `
`/usr/bin/logger #{src} #{dir}`

