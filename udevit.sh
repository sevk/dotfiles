#!/bin/bash
#e2label /dev/sda1
#tune2fs -l /dev/sda1
#udevadm info --query=all --name=/dev/sda1
#blkid /dev/
{
  #udev has no path
    PATH="/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/bin"
    # Log beggining of backup
    /usr/bin/logger USB Backup $0 $1 $2 - Beginning at `date`
    # if needed, create the mount directory
    /usr/local/bin/my_mount.rb $1 $0

    /usr/bin/logger USB Backup - End at `date`
} &

